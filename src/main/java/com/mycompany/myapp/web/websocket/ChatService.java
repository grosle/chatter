package com.mycompany.myapp.web.websocket;

import com.mycompany.myapp.domain.ChatMessageEntity;
import com.mycompany.myapp.repository.ChatMessageRepository;
import com.mycompany.myapp.repository.UserRepository;
import com.mycompany.myapp.web.websocket.dto.ChatMessageDto;
import com.mycompany.myapp.web.websocket.mapper.ChatMessageMapper;
import java.security.Principal;
import java.util.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class ChatService {

    private static final Logger log = LoggerFactory.getLogger(ChatService.class);

    private final UserRepository userRepository;
    private final ChatMessageRepository chatMessageRepository;

    @Autowired
    private ChatMessageMapper chatMessageMapper;

    public ChatService(ChatMessageRepository chatMessageRepository, UserRepository userRepository) {
        this.chatMessageRepository = chatMessageRepository;
        this.userRepository = userRepository;
    }

    @MessageMapping("/topic/message")
    @SendTo("/topic/chat")
    public ChatMessageDto sendActivity(@Payload ChatMessageDto message, Principal principal) {
        return userRepository
            .findOneByLogin(principal.getName())
            .map(user -> chatMessageMapper.toChatMessageEntity(message, user))
            .map(chatMessageRepository::save)
            .map(mapToDto(message))
            .orElseThrow();
    }

    // TODO put in mapper
    private Function<ChatMessageEntity, ChatMessageDto> mapToDto(ChatMessageDto message) {
        return e -> {
            log.debug("Sending user tracking data {}", message);
            return new ChatMessageDto(e.getId(), e.getUser().getId(), e.getUser().getLogin(), e.getMessage());
        };
    }
}
