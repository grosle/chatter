package com.mycompany.myapp.web.websocket.mapper;

import com.mycompany.myapp.domain.ChatMessageEntity;
import com.mycompany.myapp.domain.UserEntity;
import com.mycompany.myapp.web.websocket.dto.ChatMessageDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public abstract class ChatMessageMapper {

    @Mapping(target = "id", source = "chatMessage.messageId")
    public abstract ChatMessageEntity toChatMessageEntity(ChatMessageDto chatMessage, UserEntity user);
}
