package com.mycompany.myapp.web.websocket.dto;

public class ChatMessageDto {

    private final Long messageId;
    private final String username;
    private final String message;

    public ChatMessageDto(Long messageId, Long userId, String username, String message) {
        this.messageId = messageId;
        this.username = username;
        this.message = message;
    }

    public Long getMessageId() {
        return messageId;
    }

    public String getMessage() {
        return message;
    }

    public String getUsername() {
        return username;
    }
}
