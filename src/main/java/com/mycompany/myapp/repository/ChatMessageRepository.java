package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.ChatMessageEntity;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ChatMessageEntity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChatMessageRepository extends JpaRepository<ChatMessageEntity, Long> {
    @Query("select chatMessage from ChatMessageEntity chatMessage where chatMessage.user.login = ?#{principal.username}")
    List<ChatMessageEntity> findByUserIsCurrentUser();
}
