import { Component, OnDestroy, OnInit } from '@angular/core';
import { ChatService } from 'app/core/chat/chat.service';
import { ChatMessage } from 'app/core/chat/chat-message.model';

@Component({
  selector: 'jhi-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit, OnDestroy {
  messages: ChatMessage[];

  constructor(private chatService: ChatService) {
    this.messages = [];
  }

  ngOnInit(): void {
    this.chatService.connect(this);
  }

  ngOnDestroy(): void {
    this.chatService.disconnect();
  }

  connect(): void {
    this.chatService.connect(this);
  }

  sendMessage(message: string): void {
    this.chatService.sendMessage(message);
  }
}
