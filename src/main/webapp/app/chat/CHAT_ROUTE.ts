import { ChatComponent } from 'app/chat/chat.component';

export const CHAT_ROUTE = {
  path: '',
  component: ChatComponent,
  data: {
    pageTitle: 'Chat',
  },
};
