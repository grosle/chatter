import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { ChatComponent } from 'app/chat/chat.component';
import { CHAT_ROUTE } from 'app/chat/CHAT_ROUTE';

@NgModule({
  imports: [SharedModule, RouterModule.forChild([CHAT_ROUTE])],
  declarations: [ChatComponent],
  entryComponents: [ChatComponent],
})
export class ChatModule {}
