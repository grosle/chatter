import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CHAT_ROUTE } from 'app/chat/CHAT_ROUTE';

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild([CHAT_ROUTE])],
})
export class ChatRoutingModule {}
