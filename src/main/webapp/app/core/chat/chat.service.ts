import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import * as SockJS from 'sockjs-client';
import * as Stomp from 'webstomp-client';

import { AuthServerProvider } from 'app/core/auth/auth-jwt.service';
import { ChatMessage } from './chat-message.model';
import { ChatComponent } from 'app/chat/chat.component';

@Injectable({
  providedIn: 'root',
})
export class ChatService {
  private stompClient: Stomp.Client | null = null;
  private connected;

  constructor(private router: Router, private authServerProvider: AuthServerProvider, private location: Location) {
    this.connected = false;
  }

  connect(component: ChatComponent): void {
    if (this.stompClient?.connected) {
      return;
    }

    // building absolute path so that websocket doesn't fail when deploying with a context path
    let url = '/websocket/tracker';
    url = this.location.prepareExternalUrl(url);
    const authToken = this.authServerProvider.getToken();
    if (authToken) {
      url += '?access_token=' + authToken;
    }
    const socket: WebSocket = new SockJS(url);
    this.stompClient = Stomp.over(socket, { protocols: ['v12.stomp'] });
    const headers: Stomp.ConnectionHeaders = {};
    this.stompClient.connect(headers, () => {
      this.connected = true;
      if (this.stompClient != null) {
        this.stompClient.subscribe('/topic/chat', (message: Stomp.Message) => {
          component.messages.push(JSON.parse(message.body));
        });
      }
    });
  }

  disconnect(): void {
    if (this.stompClient != null) {
      this.stompClient.disconnect();
    }
  }

  sendMessage(message: string): void {
    if (this.stompClient?.connected) {
      this.stompClient.send(
        '/topic/message', // destination
        JSON.stringify(new ChatMessage(null, null, message)), // body
        {} // header
      );
    }
  }
}
