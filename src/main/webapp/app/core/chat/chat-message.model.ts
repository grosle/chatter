export class ChatMessage {
  constructor(public messageId: number | null, public username: string | null, public message: string) {}
}
