import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IChatMessage } from '../chat-message.model';
import { ChatMessageService } from '../service/chat-message.service';
import { ChatMessageDeleteDialogComponent } from '../delete/chat-message-delete-dialog.component';

@Component({
  selector: 'jhi-chat-message',
  templateUrl: './chat-message.component.html',
})
export class ChatMessageComponent implements OnInit {
  chatMessages?: IChatMessage[];
  isLoading = false;

  constructor(protected chatMessageService: ChatMessageService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.chatMessageService.query().subscribe(
      (res: HttpResponse<IChatMessage[]>) => {
        this.isLoading = false;
        this.chatMessages = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IChatMessage): number {
    return item.id!;
  }

  delete(chatMessage: IChatMessage): void {
    const modalRef = this.modalService.open(ChatMessageDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.chatMessage = chatMessage;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
