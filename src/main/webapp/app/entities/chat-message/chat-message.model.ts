import { IUser } from 'app/entities/user/user.model';

export interface IChatMessage {
  id?: number;
  message?: string;
  user?: IUser | null;
}

export class ChatMessage implements IChatMessage {
  constructor(public id?: number, public message?: string, public user?: IUser | null) {}
}

export function getChatMessageIdentifier(chatMessage: IChatMessage): number | undefined {
  return chatMessage.id;
}
