import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IChatMessage, ChatMessage } from '../chat-message.model';

import { ChatMessageService } from './chat-message.service';

describe('Service Tests', () => {
  describe('ChatMessage Service', () => {
    let service: ChatMessageService;
    let httpMock: HttpTestingController;
    let elemDefault: IChatMessage;
    let expectedResult: IChatMessage | IChatMessage[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(ChatMessageService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        message: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a ChatMessage', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new ChatMessage()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a ChatMessage', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            message: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a ChatMessage', () => {
        const patchObject = Object.assign({}, new ChatMessage());

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of ChatMessage', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            message: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a ChatMessage', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addChatMessageToCollectionIfMissing', () => {
        it('should add a ChatMessage to an empty array', () => {
          const chatMessage: IChatMessage = { id: 123 };
          expectedResult = service.addChatMessageToCollectionIfMissing([], chatMessage);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(chatMessage);
        });

        it('should not add a ChatMessage to an array that contains it', () => {
          const chatMessage: IChatMessage = { id: 123 };
          const chatMessageCollection: IChatMessage[] = [
            {
              ...chatMessage,
            },
            { id: 456 },
          ];
          expectedResult = service.addChatMessageToCollectionIfMissing(chatMessageCollection, chatMessage);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a ChatMessage to an array that doesn't contain it", () => {
          const chatMessage: IChatMessage = { id: 123 };
          const chatMessageCollection: IChatMessage[] = [{ id: 456 }];
          expectedResult = service.addChatMessageToCollectionIfMissing(chatMessageCollection, chatMessage);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(chatMessage);
        });

        it('should add only unique ChatMessage to an array', () => {
          const chatMessageArray: IChatMessage[] = [{ id: 123 }, { id: 456 }, { id: 39943 }];
          const chatMessageCollection: IChatMessage[] = [{ id: 123 }];
          expectedResult = service.addChatMessageToCollectionIfMissing(chatMessageCollection, ...chatMessageArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const chatMessage: IChatMessage = { id: 123 };
          const chatMessage2: IChatMessage = { id: 456 };
          expectedResult = service.addChatMessageToCollectionIfMissing([], chatMessage, chatMessage2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(chatMessage);
          expect(expectedResult).toContain(chatMessage2);
        });

        it('should accept null and undefined values', () => {
          const chatMessage: IChatMessage = { id: 123 };
          expectedResult = service.addChatMessageToCollectionIfMissing([], null, chatMessage, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(chatMessage);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
