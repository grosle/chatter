import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IChatMessage, getChatMessageIdentifier } from '../chat-message.model';

export type EntityResponseType = HttpResponse<IChatMessage>;
export type EntityArrayResponseType = HttpResponse<IChatMessage[]>;

@Injectable({ providedIn: 'root' })
export class ChatMessageService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/chat-messages');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(chatMessage: IChatMessage): Observable<EntityResponseType> {
    return this.http.post<IChatMessage>(this.resourceUrl, chatMessage, { observe: 'response' });
  }

  update(chatMessage: IChatMessage): Observable<EntityResponseType> {
    return this.http.put<IChatMessage>(`${this.resourceUrl}/${getChatMessageIdentifier(chatMessage) as number}`, chatMessage, {
      observe: 'response',
    });
  }

  partialUpdate(chatMessage: IChatMessage): Observable<EntityResponseType> {
    return this.http.patch<IChatMessage>(`${this.resourceUrl}/${getChatMessageIdentifier(chatMessage) as number}`, chatMessage, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IChatMessage>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IChatMessage[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addChatMessageToCollectionIfMissing(
    chatMessageCollection: IChatMessage[],
    ...chatMessagesToCheck: (IChatMessage | null | undefined)[]
  ): IChatMessage[] {
    const chatMessages: IChatMessage[] = chatMessagesToCheck.filter(isPresent);
    if (chatMessages.length > 0) {
      const chatMessageCollectionIdentifiers = chatMessageCollection.map(chatMessageItem => getChatMessageIdentifier(chatMessageItem)!);
      const chatMessagesToAdd = chatMessages.filter(chatMessageItem => {
        const chatMessageIdentifier = getChatMessageIdentifier(chatMessageItem);
        if (chatMessageIdentifier == null || chatMessageCollectionIdentifiers.includes(chatMessageIdentifier)) {
          return false;
        }
        chatMessageCollectionIdentifiers.push(chatMessageIdentifier);
        return true;
      });
      return [...chatMessagesToAdd, ...chatMessageCollection];
    }
    return chatMessageCollection;
  }
}
