import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IChatMessage, ChatMessage } from '../chat-message.model';
import { ChatMessageService } from '../service/chat-message.service';

@Injectable({ providedIn: 'root' })
export class ChatMessageRoutingResolveService implements Resolve<IChatMessage> {
  constructor(protected service: ChatMessageService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IChatMessage> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((chatMessage: HttpResponse<ChatMessage>) => {
          if (chatMessage.body) {
            return of(chatMessage.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ChatMessage());
  }
}
