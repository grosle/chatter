import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IChatMessage } from '../chat-message.model';
import { ChatMessageService } from '../service/chat-message.service';

@Component({
  templateUrl: './chat-message-delete-dialog.component.html',
})
export class ChatMessageDeleteDialogComponent {
  chatMessage?: IChatMessage;

  constructor(protected chatMessageService: ChatMessageService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.chatMessageService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
