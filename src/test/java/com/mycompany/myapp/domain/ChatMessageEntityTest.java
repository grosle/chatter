package com.mycompany.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ChatMessageEntityTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ChatMessageEntity.class);
        ChatMessageEntity chatMessageEntity1 = new ChatMessageEntity();
        chatMessageEntity1.setId(1L);
        ChatMessageEntity chatMessageEntity2 = new ChatMessageEntity();
        chatMessageEntity2.setId(chatMessageEntity1.getId());
        assertThat(chatMessageEntity1).isEqualTo(chatMessageEntity2);
        chatMessageEntity2.setId(2L);
        assertThat(chatMessageEntity1).isNotEqualTo(chatMessageEntity2);
        chatMessageEntity1.setId(null);
        assertThat(chatMessageEntity1).isNotEqualTo(chatMessageEntity2);
    }
}
